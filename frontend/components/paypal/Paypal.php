<?php

/**
 * File Paypal.php.
 *
 * @see https://github.com/paypal/rest-api-sdk-php/blob/master/sample/
 * @see https://developer.paypal.com/webapps/developer/applications/accounts
 */

namespace frontend\components\paypal;

define('PP_CONFIG_PATH', __DIR__);

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use PayPal\Api\Address;
use PayPal\Api\CreditCard;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\FundingInstrument;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\RedirectUrls;
use PayPal\Rest\ApiContext;


class Paypal extends Component
{
    //region Mode (production/development)
    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE = 'live';
    //endregion

    //region Log levels
    /*
     * Logging level can be one of FINE, INFO, WARN or ERROR.
     * Logging is most verbose in the 'FINE' level and decreases as you proceed towards ERROR.
     */
    const LOG_LEVEL_FINE = 'FINE';
    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_WARN = 'WARN';
    const LOG_LEVEL_ERROR = 'ERROR';
    //endregion

    //region API settings
    public $clientId;
    public $clientSecret;
    public $isProduction = false;
    public $currency = 'USD';
    public $config = [];

    /** @var ApiContext */
    private $_apiContext = null;

    /**
     * @setConfig
     * _apiContext in init() method
     */
    public function init()
    {
        $this->setConfig();
    }

    /**
     * @inheritdoc
     */
    private function setConfig()
    {
        // ### Api context
        // Use an ApiContext object to authenticate
        // API calls. The clientId and clientSecret for the
        // OAuthTokenCredential class can be retrieved from
        // developer.paypal.com
        $this->_apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->clientId,
                $this->clientSecret
            )
        );
        // #### SDK configuration
        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file
        // based configuration
//        $this->_apiContext->setConfig(ArrayHelper::merge(
//            [
//                'mode'                      => self::MODE_SANDBOX, // development (sandbox) or production (live) mode
//                'http.ConnectionTimeOut'    => 30,
//                'http.Retry'                => 1,
//                'log.LogEnabled'            => YII_DEBUG ? 1 : 0,
//                'log.FileName'              => Yii::getAlias('@runtime/logs/paypal.log'),
//                'log.LogLevel'              => self::LOG_LEVEL_FINE,
//                'validation.level'          => 'log',
//                'cache.enabled'             => 'true'
//            ],$this->config)
//        );
        // Set file name of the log if present
        if (isset($this->config['log.FileName'])
            && isset($this->config['log.LogEnabled'])
            && ((bool)$this->config['log.LogEnabled'] == true)
        ) {
            $logFileName = \Yii::getAlias($this->config['log.FileName']);
            if ($logFileName) {
                if (!file_exists($logFileName)) {
                    if (!touch($logFileName)) {
                        throw new ErrorException('Can\'t create paypal.log file at: ' . $logFileName);
                    }
                }
            }
            $this->config['log.FileName'] = $logFileName;
        }
        return $this;
    }

    //Demo
    public function payDemo()
    {
//        $addr = new Address();
//        $addr->setLine1("3909 Witmer Road")
//            ->setLine2("Niagara Falls")
//            ->setCity("Niagara Falls")
//            ->setState("NY")
//            ->setPostalCode("14305")
//            ->setCountryCode("US")
//            ->setPhone("716-298-1822");
//
//        $paymentCard = new CreditCard();
//        $paymentCard->setType("visa")
//            ->setNumber("4417119669820331")
//            ->setExpireMonth("11")
//            ->setExpireYear("2019")
//            ->setCvv2("012")
//            ->setFirstName("Joe")
//            ->setLastName("Shopper")
//            ->setBillingAddress($addr);
//
//        $fi = new FundingInstrument();
//        $fi->setCreditCard($paymentCard);
//
//        $payer = new Payer();
////        $payer->setPaymentMethod('credit_card');
//        $payer->setPaymentMethod('paypal');
//        $payer->setFundingInstruments(array($fi));
//
//        $amountDetails = new Details();
//        $amountDetails->setSubtotal('15.99');
//        $amountDetails->setTax('0.03');
//        $amountDetails->setShipping('0.03');
//
//        $amount = new Amount();
//        $amount->setCurrency('USD');
//        $amount->setTotal('7.47');
//        $amount->setDetails($amountDetails);
//
//        $transaction = new Transaction();
//        $transaction->setAmount($amount);
//        $transaction->setDescription('This is the payment transaction description.');
//
//        $payment = new Payment();
////        $payment->setIntent('sale');
//        $payment->setIntent('authorize');
//        $payment->setPayer($payer);
//        $payment->setTransactions(array($transaction));

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName('Ground Coffee 40 oz')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku("123123")
            ->setPrice(7.5);

        $item2 = new Item();
        $item2->setName('Granola bars')
            ->setCurrency('USD')
            ->setQuantity(5)
            ->setSku("321321")
            ->setPrice(2);

        $itemList = new ItemList();
        $itemList->setItems(array($item1, $item2));

        $details = new Details();
        $details->setShipping(1.2)
            ->setTax(1.3)
            ->setSubtotal(17.50);

        $amount = new Amount();

        $amount->setCurrency("USD")
            ->setTotal(20)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $baseUrl = Yii::$app->request->getUserIP();

        $redirectUrls = new RedirectUrls();

        $redirectUrls->setReturnUrl("http://{$baseUrl}/site/download?success=true")
            ->setCancelUrl("http://{$baseUrl}/site/download?success=false");

        $payment = new Payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        return $payment->create($this->_apiContext);
    }

}